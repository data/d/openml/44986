# OpenML dataset: nyc_taxi_green

https://www.openml.org/d/44986

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The dataset includes New York City Taxi and Limousine Commission (TLC) trips of the green line in December 2016. All trips are paid with a credit card leaving some tip.

The variable 'tip_amount' was chosen as target variable.

**Attribute Description**

1. *VendorID* - A code indicating the LPEP provider that provided the record. 1: Creative Mobile Technologies, LLC; 2: VeriFone Inc.
2. *store_and_fwd_flag*
3. *RatecodeID*
4. *PULocationID*
5. *DOLocationID* - TLC Taxi Zone in which the taximeter was disengaged.
6. *passenger_count* - the number of passengers in the vehicle. This is a driver-entered value
7. *extra* - miscellaneous extras and surcharges. Currently, this only includes the $0.50 and $1 rush hour and overnight charges
8. *mta_tax* - $0.50 MTA tax that is automatically triggered based on the metered rate in use.
9. *tip_amount* - target feature
10. *tolls_amount*
11. *improvement_surcharge* - $0.30 improvement surcharge assessed on hailed trips at the flag drop
12. *total_amount*
13. *trip_type* - 1: Street-hail, 2: Dispatch
14. *lpep_pickup_datetime_day*
15. *lpep_pickup_datetime_hour*
16. *lpep_pickup_datetime_minute*
17. *lpep_dropoff_datetime_day*
18. *lpep_dropoff_datetime_hour*
19. *lpep_dropoff_datetime_minute*

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44986) of an [OpenML dataset](https://www.openml.org/d/44986). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44986/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44986/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44986/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

